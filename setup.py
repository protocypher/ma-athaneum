from setuptools import setup


setup(
      name="Athaneum",
      version="0.1.0",
      packages=["athaneum"],
      url="https://bitbucket.org/protocypher/ma-athaneum",
      license="MIT",
      author="Benjamin Gates",
      author_email="benjamin@snowmantheater.com",
      description="A library categlog management application."
)

