# Athaneum

**Library Catalog**

Athaneum is a basic, text based, book lending application that uses the
`cmd.Cmd` framework. It manages a collection of books and media, all with
variaous lending rules, and allowes users to check out and return books.
It can also provide various reports on the status of the library.

